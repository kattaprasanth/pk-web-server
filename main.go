package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// MsgHandler to handler the messages
type MsgHandler struct {
	Message string
}

// ServeHTTP method
func (m *MsgHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(m.Message))
}

// barHandler, function handler calling example
func barHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Bar calling using FunctionHandlers.."))
}

// Product in the inventory
type Product struct {
	ProductID      int    `json:"productId"`
	Manufacturer   string `json:"manufacturer"`
	Sku            string `json:"sku"`
	Upc            string `json:"upc"`
	PricePerUnit   string `json:"pricePerUnit"`
	QuantityOnHand int    `json:"quantityOnHand"`
	ProductName    string `json:"productName"`
}

var productList []Product

func init() {
	productsJSON := `[
	{
		"ProductId": 1,
		"Manufacturer": "PK-Jenkins",
		"Sku": "pzxy123",
		"Upc": "934567",
		"PricePerUnit": "497.95",
		"QuantityOnHand": 970,
		"ProductName": "Jenkins CI/CD Tool"
	},
	{
		"ProductId": 2,
		"Manufacturer": "PK-Packer",
		"Sku": "pzxy1234",
		"Upc": "9345671",
		"PricePerUnit": "1000.95",
		"QuantityOnHand": 9701,
		"ProductName": "Immutetable Infra"
	},
	{
		"ProductId": 3,
		"Manufacturer": "PK-tf",
		"Sku": "pzxy1235",
		"Upc": "9345672",
		"PricePerUnit": "2000.00",
		"QuantityOnHand": 9702,
		"ProductName": "Infra Auto"
	}
	]`
	err := json.Unmarshal([]byte(productsJSON), &productList)
	if err != nil {
		log.Fatal(err)
	}
}

// return next highest productID
func getNextID() int {
	highestID := -1
	for _, product := range productList {
		if highestID < product.ProductID {
			highestID = product.ProductID
		}
	}
	return highestID + 1
}

func productHandler(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, "products/")
	productID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	product, listIteamIndex := findProductByID(productID)
	if product == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	switch r.Method {
	case http.MethodGet:
		productJSON, err := json.Marshal(product)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(productJSON)
	case http.MethodPut:
		var updateProduct Product
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = json.Unmarshal(bodyBytes, &updateProduct)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if updateProduct.ProductID != productID {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		product = &updateProduct
		productList[listIteamIndex] = *product
		w.WriteHeader(http.StatusOK)
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

func findProductByID(productID int) (*Product, int) {
	for i, product := range productList {
		if product.ProductID == productID {
			return &product, i
		}
	}
	return nil, 0
}

func productsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		productsJson, err := json.Marshal(productList)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(productsJson)
	case http.MethodPost:
		var newProduct Product
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		err = json.Unmarshal(bodyBytes, &newProduct)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if newProduct.ProductID != 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		newProduct.ProductID = getNextID()
		productList = append(productList, newProduct)
		w.WriteHeader(http.StatusCreated)
		return
	}
}

func main() {
	http.Handle("/status", &MsgHandler{Message: "Server Up and Running."})
	http.HandleFunc("/products", productsHandler)
	http.HandleFunc("/products/", productHandler)
	http.HandleFunc("/bar", barHandler)
	http.ListenAndServe(":5000", nil)
}
